FROM registry.gitlab.com/craynic.com/docker/lap:0

WORKDIR /var/www/localhost/htdocs

RUN git clone --recurse-submodules https://github.com/LycheeOrg/Lychee-Laravel.git . \
    && composer install --no-dev

# copy files
COPY files/ /

ENV MYSQL_HOST="mysql" \
    MYSQL_PORT="3306" \
    MYSQL_DATABASE="lychee" \
    MYSQL_USERNAME="lychee" \
    MYSQL_PASSWORD="lychee"

# increase memory limit in PHP
RUN cp .env.example .env \
    && sed -i "s/^DB_HOST=.*$/DB_HOST=$MYSQL_HOST/" .env \
    && sed -i "s/^DB_PORT=.*$/DB_HOST=$MYSQL_PORT/" .env \
    && sed -i "s/^DB_DATABASE=.*$/DB_HOST=$MYSQL_DATABASE/" .env \
    && sed -i "s/^DB_USERNAME=.*$/DB_HOST=$MYSQL_USERNAME/" .env \
    && sed -i "s/^DB_PASSWORD=.*$/DB_HOST=$MYSQL_PASSWORD/" .env \
    && sed -i "s/^APP_DEBUG=.*$/APP_DEBUG=false/" .env \
    && chmod -R 775 storage/* app/* public/uploads public/sym public/dist \
    && chown -R www-data:www-data storage/* app/* public/uploads public/sym public/dist \
    && php artisan key:generate
